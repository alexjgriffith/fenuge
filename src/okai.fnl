(local okai {})

(global canvas (js.global.document:getElementById :canvas))
(global ctx (canvas:getContext :2d))
(global mousetrap js.global.Mousetrap)
(global window js.global.window)
;; (global pixi js.global.PIXI)
;; (global howler js.global.Howler)

(local render (require :render))
(local map (require :map))

(tset okai :state {:reset true})
(local default-state
       {:map
        {:height 160
         :width 160
         :tile-size 16
         :tile-width 10
         :tile-hight 10}
        :player
        {:x 3
         :y 2}
        :monsters [{:name :oger :char :O :x 4 :y 4 :collide true}
                   {:name :priest :char :P :x 5 :y 7 :collide true}]
        :text {:x 0 :y 0 :size 20 :font "200 20px Fira Mono"}
        :camera
        {:x 0
         :y 0}
        :running-state :stopped
        :initialized true
        :reset nil
        :reset-callback (fn [] nil)
        })

(fn tick []
  (local state okai.state)
  (render.render state))

(fn merge-table [table-a table-b]
  "Overwrite values of table-a with those of table-b."
  (each [key value (pairs table-b)]
    (if (and (= "table" (type value))
             (= "table" (type (. table-a key))))
        (merge-table (. table-a key) value)
        (tset table-a key value)))
  table-a)

(fn okai.reload-lib []
  (_r :okai)
  (_r :render)
  (_r :map))

(tset okai :version "0.1.5")

(fn collide-monsters [x y]
  (local player okai.state.player)
  (local monsters okai.state.monsters)
  (var ret false)
  (each [_ monster (ipairs monsters)]
    (when (and (= x monster.x) (= y monster.y) monster.collide)
      (print (.. "Collided with " monster.name))
      (set ret true)))
  ret)

(fn move [valx valy]
  (local player okai.state.player)
  (when (and (not (map.can-move
                   map.level1 map.collidables
                   (+ player.x 1 valx)
                   (+ player.y 1 valy)))
             (not (collide-monsters
                   (+ player.x valx)
                   (+ player.y valy))))
    (tset okai.state.player :y (+ okai.state.player.y valy))
    (tset okai.state.player :x (+ okai.state.player.x valx)))
  (tick))

(fn okai.move-up [] (move 0 -1))
(fn okai.move-down [] (move 0 1))
(fn okai.move-left [] (move -1 0))
(fn okai.move-right [] (move 1 0))

(tset okai :controls {:w :move-up
                      :s :move-down
                      :a :move-left
                      :d :move-right
                      :r :reload-lib})

(fn setup-controls [controls]
  (each [key value (pairs controls)]
    (mousetrap:bind key (. okai value))))

(fn okai.init []
  (if (not okai.state.initialized)
      (do
          (setup-controls okai.controls)
        (merge-table okai.state default-state)
        (tset okai.state :initialized true)
        (tset okai.state :reset nil)
        (tick)
        :ok)
      (values :err "State already initialized.")))
okai

(local level1
       ["--------------------"
        "|.|...|............|"
        "|.|...|.....###....|"
        "|.-.---.....###....|"
        "|..................|"
        "|..................|"
        "|.............~~~~.|"
        "|..............~~~.|"
        "|...............~~.|"
        "--------------------"])

(fn index-map [level x y]
  (string.sub (. level y) x x))

(fn can-move [level collidables x y]
  (. collidables (index-map level x y)))

{:level1 level1 :collidables {"-" true "|" true " " true "~" true}
 :index-map index-map :can-move can-move}

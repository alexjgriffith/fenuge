(local render {})

(fn render.clear [state]
  (ctx:clearRect 0 0 canvas.width canvas.height))

(fn render.monster [monster]
  (ctx:clearRect (* monster.x 12) (* monster.y 20) 12 20)
  (ctx:fillText monster.char (* monster.x 12) (+ 20 (* 20 monster.y)))
  ;; (ctx:beginPath)
  ;; (ctx:rect player.x player.y 10 10)
  ;; (ctx:stroke)
  )


;; (render.player {:player {:x 3 :y 3}})
(fn render.player [state]
  (local player state.player)
  (ctx:clearRect (* player.x 12) (* player.y 20) 12 20)
  (ctx:fillText "@" (* player.x 12) (+ 20 (* 20 player.y)))
  ;; (ctx:beginPath)
  ;; (ctx:rect player.x player.y 10 10)
  ;; (ctx:stroke)
  )

(fn render.monsters [state]
  (local monsters state.monsters)
  (each [_ monster (ipairs monsters)]
         (render.monster monster)))

(fn render.text [state]
  (local text state.text)
  (let [x text.x
        y text.y
        size text.size
        font text.font]
    (tset ctx :font text.font)
    (tset ctx :textAlign "left")
    (tset ctx :verticalAlign "top")
    (for [i 1 (# state.level)]
        (ctx:fillText (. state.level i) x (+ (* i size) y)))
    ;; (ctx:fillText "HIJKLMNOP" x (+ (* 2 size) y))
    (local sample (ctx:measureText "A"))
    ;; (js.global.console.log (ctx:measureText "Hello world!"))
    (tset text :width (.  sample :width))
    text))


;; (render.text-debug {:text {:x 0 :y 0 :size 20 :font "200 20px Fira Mono"}})
;; ctx.font = "600 54px Arial";
;; ctx.font = "600 20px Fira Mono";
(fn render.text-debug [state]
  (local map (require :map))
  (_r :map map {})
  (_r :render render {})
  (tset state :level map.level1)
  (render.clear)
  (render.text state)
  ;; (render.player state)
  )

(fn render.render [state]
  (local map (require :map))
  (tset state :level map.level1)
  (render.clear)
  (render.text state)
  (render.monsters state)
  (render.player state))

render

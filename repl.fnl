(local js (require "js"))
;; (local fennel (require "lib.fennel"))
(local log (fn [...] (: js.global.console :log ...)))


(fn reload-module [module-keyword]
  (let [old (require module-keyword)
        _ (tset package.loaded module-keyword nil)
        (ok new) (pcall require module-keyword)
        ;; keep the old module if reload failed
        new (if (not ok) (do (print new) old) new)]
    ;; if the module isn't a table then we can't make
    ;; changes which affect already-loaded code, but if
    ;; it is then we should splice new values into the
    ;; existing table and remove values that are gone.
    (when (= (type new) :table)
      (each [k v (pairs new)]
        (tset old k v))
      (each [k (pairs old)]
        ;; the elisp reader is picky about where . can be
        (when (not (. new k))
          (tset old k nil)))
      (tset package.loaded module-keyword old))
    (values :ok old)))

(global _r (fn [module-keyword] (reload-module module-keyword)))

(var last-input nil)
(var last-value nil)

(local friend (require :lib.fennelfriend))

(partial fennel.repl {:moduleName "lib.fennel"
                      :readChunk (fn []
                                   (let [input (coroutine.yield)]
                                     (set last-input input)
                                     (print (.. "> " input))
                                     (.. input "\n")))
                      :onValues (fn [xs]
                                  (print (table.concat xs "\t"))
                                  (set last-value (. xs 1)))
                      ;; TODO: make errors red
                      ;; TODO: log errors for analysis?
                      :onError (fn [_ msg] (printError msg))
                      :pp (require :lib.fennelview)
                      :assert-compile friend.assert-compile
                      :parse-error friend.parse-error
                      :env nil})
